# README/Quickstart #

1st: remember to sign up to create your account, and add your app information and ad placement.
 
Unless you have one we cannot track your ad impressions, clicks, etc. You can still implement the ads, but not going live.

[![LandingPage 2016-02-15 10-19-32.png](https://bitbucket.org/repo/o4ppAg/images/3569750337-LandingPage%202016-02-15%2010-19-32.png)](http://nativeads.pocketmedia.mobi/signup.html)


# What is this repository for? 

* Summary: this project contains the source code for the Android library for Pocket Media Native Ads. The library lets you integrate the ads without effort, and being able to customize everything about them.
* Current Version: 1.2.3 (in active development, you can check latest version in [Maven Central](http://search.maven.org/#search%7Cga%7C1%7Ca%3A%22nativeadslib%22), and also download sources.jar file and the aar)

# Can I use the library without building the source?

Sure thing. We have published the ```aar``` artifact in Maven Central. This repository is already included in the deafault ```jcenter()``` repository. In case you don't have it, you might need to add the repo:

```
repositories {
    jcenter()
}
```

And the library dependency:

```
 dependencies {
     compile 'mobi.pocketmedia:nativeadslib:1.2.3'
 }
```

To know about the different methods available and how to start, take a look to the sample project.

# How do I get set up?

The project is made for Android Studio. Once imported, you will find two different modules: the demoApp, and the nativeAds library project.

![ProjectOverview.png](https://bitbucket.org/repo/o4ppAg/images/1840699795-ProjectOverview.png)


# Sample project / Usage 

## Downloading ads

In this repo you will find an example of the usage of the NativeAds library in your own app. It's intended to be totally customizable by you, and that's why provide you the source code. The only thing we need from you is you to download a JSON file with our offers, and display them by opening the click URL we provide you.

That's what this library makes easy:

```
#!java

    /**
     * Method to download the ads. Initializing the ad request.
     */
    private void downloadAds() {
        Log.d(getClass().getSimpleName(), "Starting Ads download");
        /**
        We download ads for:
        - application context
        - 3 ads
        - placement id 4bea87cd5549527b10d2886061121d0f8e22071b (you need to replace this with your own ad placement token!)
        - debug mode enabled - disable in production!
         */
        nativeAdRequest = new NativeAdRequest(
                getActivity().getApplicationContext(),
                3,
                "4bea87cd5549527b10d2886061121d0f8e22071b" /* replace this with your own! */
                , true
        );
        nativeAdRequest.initAdRequest(this);
    }

```

Make sure you modify the ad placement token (**4bea87cd5549527b10d2886061121d0f8e22071b**). Otherwise we cannot track your ad displays or conversions!

And after that, we provide you with three callbacks to be aware of the status of your ads request, as you can check in the ```NativeAdsRequestListener```: 

1. onAdRequestStarted
2. onAdRequestSuccess
3. onAdRequestError

In the usual situation, you will be displaying the ads in the onAdRequestSuccess, with the layout of your choice (provided by the library, or customised).

## Displaying an ad

To display the Ad, you will want to take a look to the ```NativeAdUnit``` class, containing the different data about the offers you will display:

- campaignName: the title of the campaign, usually a few words
- campaignDescription: the description of the campaign, usually a short paragraph with a few lines of text
- clickURL: the URL that must be opened when the user clicks in the ad
- appStoreUrl: the preview URL, where the user will be taken. It could be empty.
- campaignImage: this property should be accessed trough the ```getImageUrl()```, as the field can come in two different attributes from the API (```campaignImage``` and ```defaultIcon```)
- images: an object holding the different assets a campaign might have (icons, high resolution icons, banners, and bigger images)

![photo18534536685136897.jpg](https://bitbucket.org/repo/o4ppAg/images/1270197010-photo18534536685136897.jpg)


## Opening an ad link ###

In the class ```NativeAdDisplayHelper``` we provide some utility methods to help you displaying the ads, and opening the links in a embedded ```WebView```.

The reason why this is done this way is because the urls of the ads point to tracking domains that will save the clicks of your users for us to know what it was your user who clicked on an ad. To improve the user experience, this process happens in-app.

The key method to open an ad in a seamless way within your app (think of the way Facebook opens their ads, for example) is this:


```
#!java

/**
     * Method to open the Google Play Store app page, with an in-app
     * browser to follow the ads redirects until the user is taken to
     * the final url, for the best possible user experience
     *
     * @param displayedAd a NativeAdUnit object
     */
    public void openAd(NativeAdUnit displayedAd) {
      ...
    }
```

By getting an instance of the ```NativeAdDisplayHelper``` class, providing an Activity, you'll be able to open the ad within the app. It will follow the redirects in a webview, and the moment the final URL is reached, if it's a Google Play one, it will open the native app.

The aim of this is making the process of showing and clicking on an ad as smooth as possible for the users, to maximize the potential conversion, while keeping a great user experience.

### Library deployment configuration (only for the library) ###

In the usual cases, you won't need to edit the library itself. You could even create a ticket for us to make the changes. But the project is open source, and you could do it, if you want to customise it even further.

The library project uses a ```gradle.properties``` file with the settings to deploy the library to the snapshots repository of your choice. This file is ignored by default, and the properties to be used are in the sample ```gradle.properties.sample```.

In order to build and publish the library to your snapshots repository, you will need to create the settings file and customize it with the correct values.

### Contribution guidelines ###

We accept contributions from external developers. You can also create your own fork of our code to customize it even more, as our library development is open and transparent, in order to save you time when implementing our ads platform.

If you don't feel like solving the problem yourself, feel free to report it to us, and we will take it to our backlog and try to solve it as soon as possible.


### I have problems: Who do I talk to? ###

We work for you - and we want to solve any problem you may have, or hear from your suggestions, recommendations, or anything that comes to your mind while using this project. Feel free to reach us any time, we'll be happy to help!

* Via email in [support@pocketmedia.mobi](mailto:support@pocketmedia.mobi)
* Via Twitter: [PocketMediaTech](http://twitter.com/PocketMediaTech)