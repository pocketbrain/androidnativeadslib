package mobi.pocketmedia.nativeadslib.images;


import mobi.pocketmedia.nativeadslib.NativeAdImage;

public class Icon extends NativeAdImage{

    @Override
    public ImageType getImageType() {
        return ImageType.icon;
    }

}
