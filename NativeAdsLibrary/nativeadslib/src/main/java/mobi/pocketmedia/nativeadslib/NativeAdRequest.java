package mobi.pocketmedia.nativeadslib;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import mobi.pocketmedia.nativeadslib.exceptions.NoAdsAvailableException;
import mobi.pocketmedia.nativeadslib.gaid.GoogleAdIdGetter;


/**
 * This class is used to perform a request to the offer engine api and retrieve a number of ads.
 *
 * Created by Pocket Media on 27-Jul-15.
 */
public class NativeAdRequest {

    /**
     * A list of NativeAd units retrieved as a result
     */
    public List<NativeAdUnit> nativeAds = new LinkedList<>();
    /**
     * Context to interact with preferences and UI
     */
    public Context myContext;
    /**
     * This debug mode will enable more detailed logging
     */
    public boolean debugMode = false;

    /**
     * The number of ads to retrieve is specified in the constructor
     */
    private int numberOfAds;
    /**
     * The identifier for the ad placement, retrieved in your user dashboard
     */
    private String adPlacementToken;

    public enum ImageTypeFilter {
        allImages(""),
        icon("icon"),
        hq_icon("hq_icon"),
        banner("banner"),
        bigImages("banner,hq_icon"),
        bannerAndIcons("banner,icon");

        private final String name;
        ImageTypeFilter(String s){
            name = s;
        }

    }

    public ImageTypeFilter imageFilter = ImageTypeFilter.allImages;


    private NativeAdsRequestListener listener;

    /**
     * The constructor of the class. Initialize the context, the number of ads requested,
     * the affiliate id and the placement of the ad.
     *
     * Debug mode will be disabled by default.
     *
     * @param context       the application's context
     * @param numberOfAds   the number of the ads you want to get
     * @param adPlacementToken the placement of the ad
     */
    public NativeAdRequest(Context context, int numberOfAds, String adPlacementToken) {
        this.myContext = context;
        this.adPlacementToken = adPlacementToken;
        this.numberOfAds = numberOfAds;
    }

        /**
         * The constructor of the class. Initialize the token if null, the number of ads requested,
         * the affiliate id and the placement of the ad.
         *
         * @param context       activity's context
         * @param numberOfAds   the number od the ads you want to get
         * @param adPlacementToken placement of the ad
         * @param debugMode     enables/disables more detailed info for development purposes
         */
    public NativeAdRequest(Context context, int numberOfAds, String adPlacementToken, boolean debugMode) {
        this.myContext = context;
        this.adPlacementToken = adPlacementToken;
        this.numberOfAds = numberOfAds;
        this.debugMode = debugMode;
    }

    /**
     * Method to initialize the native ad request.
     * It starts the execution of the NativeAdAsync task and
     * notifies that the request has started.
     *
     * @param listener the callback listener of the native ad request
     */
    public void initAdRequest(NativeAdsRequestListener listener) {

        this.listener = listener;

        NativeAdAsync nativeAdsAsync = new NativeAdAsync();
        nativeAdsAsync.execute(listener);

        if (listener != null) {
            listener.onAdRequestStarted(this);
        }
    }

    /**
     * Method to set the nativeAdsList
     *
     * @param nativeAdsList A list of native ads
     */
    public void setNativeAds(List<NativeAdUnit> nativeAdsList) {
        this.nativeAds = nativeAdsList;
    }

    /**
     * Method to get the nativeAdsList
     *
     * @return A list of native ads
     */
    public List<NativeAdUnit> getNativeAds() {
        return this.nativeAds;
    }

    /**
     * This class extends Async Task and is used to parse the native ads from serverURL (url to the offer engine api).
     * The parameter of the async task is the callback listener of the native ad request.
     *
     */
    public class NativeAdAsync extends AsyncTask<NativeAdsRequestListener, Void, List<NativeAdUnit>> {

        public String serverURL;

        List<NativeAdUnit> nativeAdsList;

        @Override
        protected List<NativeAdUnit> doInBackground(NativeAdsRequestListener... params) {

            try {
                URL url = new URL(getServerUrl(adPlacementToken, numberOfAds));

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setUseCaches(false);
                urlConnection.setRequestMethod("GET");

                if (debugMode) {
                    Log.d(getClass().getSimpleName(), "Offer engine url: " + serverURL);
                }

                if (urlConnection.getResponseCode() == NativeAdsConstants.STATUS_OK) {
                    InputStream content = null;

                    try {
                        content = new BufferedInputStream(urlConnection.getInputStream());
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onAdRequestError(NativeAdRequest.this, e);
                        }
                    }

                    if (content != null) {

                        try {
                            //Read the server response and attempt to parse it as JSON
                            Reader reader = new InputStreamReader(content);

                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                            Gson gson = gsonBuilder.setLenient().create();

                            JsonParser parser = new JsonParser();
                            JsonArray array = parser.parse(reader).getAsJsonArray();
                            NativeAdUnit[] adsCollection = gson.fromJson(array, NativeAdUnit[].class);
                            nativeAdsList = Arrays.asList(adsCollection);


                            if (nativeAdsList.size() < 1) {
                                if (listener != null) {
                                    listener.onAdRequestError(NativeAdRequest.this, new NoAdsAvailableException());
                                }
                            }

                            content.close();


                        } catch (Exception ex) {
                            errorJSONParse(ex, NativeAdsConstants.NATIVE_AD_REQUEST);
                            if (listener != null) {
                                listener.onAdRequestError(NativeAdRequest.this, ex);
                            }
                        }

                        setNativeAds(nativeAdsList);

                    }
                } else {
                    if (debugMode) {
                        Log.e(NativeAdsConstants.NATIVE_AD_REQUEST, "Server responded with status code: " + urlConnection.getResponseCode());
                    }
                    listener.onAdRequestError(NativeAdRequest.this, new RuntimeException());

                }
            } catch (Exception ex) {
                if (debugMode) {
                    Log.e(NativeAdsConstants.NATIVE_AD_REQUEST, "Failed to send HTTP POST request due to: " + ex);
                }
                assert listener != null;
                listener.onAdRequestError(NativeAdRequest.this, ex);

            }
            return null;
        }

        @Override
        protected void onPostExecute(List<NativeAdUnit> nativeAdsAttributes) {
            if (listener != null && nativeAdsList != null) {
                listener.onAdRequestSuccess(NativeAdRequest.this, nativeAdsList);
            }
        }

        private void errorJSONParse(Exception ex, String TAG) {
            if (debugMode) {
                Log.e(TAG, "Failed to parse JSON due to: " + ex);
            }
            ex.printStackTrace();
        }

        /**
         * Method to set the url that is used to connect to the offer engine api.
         *
         * @param placementID your placement id
         * @param limit number of ads
         * @return the url as a string
         * @throws GooglePlayServicesNotAvailableException
         */
        public String getServerUrl(String placementID, int limit) throws GooglePlayServicesNotAvailableException {

            /**
             * Getting the Google Advertising Identifier of the device
             */
            if (myContext == null) {
                throw new NullPointerException();
            }
            GoogleAdIdGetter googleAdIdGetter = new GoogleAdIdGetter(myContext);
            String googleAdId = googleAdIdGetter.getGoogleAdID();
            String model = "unknown";
            try {
                model = NativeAdsConstants.DEVICE_MODEL;
                model = URLEncoder.encode(model, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            String imageTypes = "";
            try{
                if (imageFilter != ImageTypeFilter.allImages) {
                    imageTypes = imageFilter.name;
                }
            }catch(Exception e){
                e.printStackTrace();
            }

             return this.serverURL = myContext.getResources().getString(R.string.offerwall_url,
                        limit, NativeAdsConstants.ANDROID_VERSION, model,
                        googleAdId, placementID, imageTypes);

        }
    }
}

