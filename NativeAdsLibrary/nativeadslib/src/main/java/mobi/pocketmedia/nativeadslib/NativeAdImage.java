package mobi.pocketmedia.nativeadslib;

import com.google.gson.annotations.SerializedName;

/**
 * This generic class holds the methods and attributes that images belonging to a NativeAdUnit have.
 * The main attribute is the URL.
 *
 * Created by adrian on 05/09/16.
 */
public class NativeAdImage {

    @SerializedName("url")
    public String url;

    @SerializedName("width")
    public float width;

    @SerializedName("height")
    public float height;

    public enum ImageType{
        icon, banner, hqIcon, fullscreen, other
    }

    private ImageType imageType = ImageType.other;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }
}
