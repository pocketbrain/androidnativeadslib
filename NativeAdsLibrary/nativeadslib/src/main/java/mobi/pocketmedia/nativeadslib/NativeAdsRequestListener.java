package mobi.pocketmedia.nativeadslib;

import java.util.List;

/**
 * Callback listener for the native ad request.
 * It contains three methods for the request starting, failing or succeeding.
 *
 * Created by Pocket Media on 17/12/15.
 */
public interface NativeAdsRequestListener {

    /**
     * Method to be implemented when the adRequest has started.
     * @param request a NativeAdRequest object
     */
    void onAdRequestStarted(NativeAdRequest request);

    /**
     * Method to be implemented when the adRequest is successful.
     * @param request a NativeAdRequest object
     * @param ads a list of NativeAdUnit objects
     */
    void onAdRequestSuccess(NativeAdRequest request, List<NativeAdUnit> ads);

    /**
     * Method to be implemented when the adRequest has failed.
     * @param request a NativeAdRequest object
     * @param error an exception to catch
     */
    void onAdRequestError(NativeAdRequest request, Exception error);

}
