package mobi.pocketmedia.nativeadslib;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * The native ad unit. This class is used as a model that contains all the variables for each
 * object of the json that offer engine api is responding. Each object of the json is a native ad and contains values
 * for these attributes. This class extends the NativeAdsItem class from which it inherits the adType attribute.
 * <p/>
 * <p/>
 * Created by Pocket Media on 5/28/2015.
 */
public class NativeAdUnit extends NativeAdsItem{

    @SerializedName("id")
    @Expose
    public String unitID;
    @SerializedName("campaign_name")
    @Expose
    public String campaignName;
    @SerializedName("campaign_description")
    @Expose
    public String campaignDescription;
    @SerializedName("app_store_url")
    @Expose
    public String appStoreUrl;
    @SerializedName("click_url")
    @Expose
    public String clickURL;
    @SerializedName("action_text")
    @Expose
    public String actionText;
    @SerializedName("campaign_image")
    @Expose
    public String campaignImage;
    @SerializedName("images")
    @Expose
    public NativeAdImages images;
    @SerializedName("open_in_browser")
    @Expose
    public Boolean shouldBeManagedExternally;

    /**
     * 
     * @return
     *     The unitID
     */
    public long getUnitID() {
        return Long.valueOf(unitID);
    }

    /**
     * 
     * @param unitID
     *     The unitID
     */
    public void setUnitID(String unitID) {
        this.unitID = unitID;
    }

    /**
     * 
     * @return
     *     The campaignName
     */
    public String getCampaignName() {
        return campaignName;
    }

    /**
     * 
     * @param campaignName
     *     The campaign_name
     */
    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    /**
     * 
     * @return
     *     The campaignDescription
     */
    public String getCampaignDescription() {
        return campaignDescription;
    }

    /**
     * 
     * @param campaignDescription
     *     The campaign_description
     */
    public void setCampaignDescription(String campaignDescription) {
        this.campaignDescription = campaignDescription;
    }

    /**
     * 
     * @return
     *     The appStoreUrl
     */
    public String getAppStoreUrl() {
        return appStoreUrl;
    }

    /**
     * 
     * @param appStoreUrl
     *     The app_store_url
     */
    public void setAppStoreUrl(String appStoreUrl) {
        this.appStoreUrl = appStoreUrl;
    }

    /**
     * 
     * @return
     *     The clickURL
     */
    public String getClickURL() {
        return clickURL;
    }

    /**
     * 
     * @param clickURL
     *     The click_url
     */
    public void setClickURL(String clickURL) {
        this.clickURL = clickURL;
    }

    /**
     * 
     * @return
     *     The actionText
     */
    public String getActionText() {
        return actionText;
    }

    /**
     * 
     * @param actionText
     *     The action_text
     */
    public void setActionText(String actionText) {
        this.actionText = actionText;
    }

    /**
     * 
     * @return
     *     The campaignImage
     */
    public String getCampaignImage() {
        return campaignImage;
    }

    /**
     * 
     * @param campaignImage
     *     The campaign_image
     */
    public void setCampaignImage(String campaignImage) {
        this.campaignImage = campaignImage;
    }

    /**
     * 
     * @return
     *     The images
     */
    public NativeAdImages getImages() {
        return images;
    }

    /**
     * 
     * @param images
     *     The images
     */
    public void setImages(NativeAdImages images) {
        this.images = images;
    }

    /**
     * 
     * @return
     *     The shouldBeManagedExternally
     */
    public Boolean getShouldBeManagedExternally() {
        return shouldBeManagedExternally;
    }

    /**
     * 
     * @param shouldBeManagedExternally
     *     The open_in_browser
     */
    public void setShouldBeManagedExternally(Boolean shouldBeManagedExternally) {
        this.shouldBeManagedExternally = shouldBeManagedExternally;
    }

    public String getImageUrl(){

        String image = "";

        if (campaignImage != null){
            image = campaignImage;
        }

        return image;
    }

}
