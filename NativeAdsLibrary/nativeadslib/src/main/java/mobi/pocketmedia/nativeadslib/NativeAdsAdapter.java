package mobi.pocketmedia.nativeadslib;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class extends the BaseAdapter class. It is used to mix the items of an existing adapter and the native ads.
 * The usage of this adapter is optional.
 * <p/>
 * Created by Pocket Media on 15-Jan-16.
 */
public class NativeAdsAdapter extends BaseAdapter {

    private Activity activity;
    protected List<NativeAdUnit> ads;
    private BaseAdapter userAdapter;
    private List<Object> mixedList = new ArrayList<>();
    private int adLayout;
    private int counter;
    private boolean followLinksInInappBrowser = true;
    private boolean debugMode = false;
    private int iconID = 0;
    private int titleID = 0;
    private int descriptionID = 0;
    private int imageID = 0;
    private int installButtonID = 0;


    /**
     * The constructor of the adapter.
     * Initialize the class parameter myAdapter with the existing adapter,
     * myActivity with the current activity, myAds with the ads retrieved and layoutResource
     * with the resource of the layout to use for each native ad. Also, it adds the items of the
     * existing adapter in a new list of objects, the mixedList.
     *
     * @param myAdapter      the existing base adapter
     * @param myActivity     the current activity
     * @param myAds          list of native ads objects
     * @param layoutResource the resource of the native ad layout
     */
    public NativeAdsAdapter(BaseAdapter myAdapter, Activity myActivity, List<NativeAdUnit> myAds, int layoutResource) {
        this.userAdapter = myAdapter;
        this.activity = myActivity;
        this.ads = myAds;
        this.adLayout = layoutResource;
        this.counter = 0;

        for (int i = 0; i < myAdapter.getCount(); i++) {
            mixedList.add(myAdapter.getItem(i));
        }

    }

    /**
     * The constructor of the adapter.
     * Initialize the class parameter myAdapter with the existing adapter,
     * myActivity with the current activity, myAds with the ads retrieved and layoutResource
     * with the resource of the layout to use for each native ad. Also, it adds the items of the
     * existing adapter in a new list of objects, the mixedList.
     *
     * @param myAdapter      the existing base adapter
     * @param myActivity     the current activity
     * @param myAds          list of native ads objects
     * @param layoutResource the resource of the native ad layout
     * @param debugMode      enables/disables more detailed info for development purposes
     */
    public NativeAdsAdapter(BaseAdapter myAdapter, Activity myActivity, List<NativeAdUnit> myAds,
                            int layoutResource, boolean debugMode) {
        this.userAdapter = myAdapter;
        this.activity = myActivity;
        this.ads = myAds;
        this.adLayout = layoutResource;
        this.counter = 0;
        this.debugMode = debugMode;

        for (int i = 0; i < myAdapter.getCount(); i++) {
            mixedList.add(myAdapter.getItem(i));
        }

    }

    /**
     * The constructor of the adapter.
     * Initialize the class parameter myAdapter with the existing adapter,
     * myActivity with the current activity, myAds with the ads retrieved and layoutResource
     * with the resource of the layout to use for each native ad. Also, it adds the items of the
     * existing adapter in a new list of objects, the mixedList. Sets values for two boolean
     * parameters in order to enable/disable to follow the links in an in-app browser and in order to
     * enable/disable to follow redirects in the background (experimental feature).
     *
     * @param myAdapter                   the existing base adapter
     * @param myActivity                  the current activity
     * @param myAds                       list of native ads objects
     * @param layoutResource              the resource of the native ad layout
     * @param followLinksInInappBrowser   enable - disable following redirects after the user has clicked install
     * @param debugMode                   enables/disables more detailed info for development purposes
     */
    public NativeAdsAdapter(BaseAdapter myAdapter, Activity myActivity, List<NativeAdUnit> myAds,
                            int layoutResource, boolean followLinksInInappBrowser, boolean debugMode) {
        this.userAdapter = myAdapter;
        this.activity = myActivity;
        this.ads = myAds;
        this.adLayout = layoutResource;
        this.followLinksInInappBrowser = followLinksInInappBrowser;
        this.counter = 0;
        this.debugMode = debugMode;

        for (int i = 0; i < myAdapter.getCount(); i++) {
            mixedList.add(myAdapter.getItem(i));
        }

    }

    /**
     * The constructor of the adapter.
     * Initialize the class parameter myAdapter with the existing adapter,
     * myActivity with the current activity, myAds with the ads retrieved and layoutResource
     * with the resource of the layout to use for each native ad. Also, it adds the items of the
     * existing adapter in a new list of objects, the mixedList. Initializes the ids
     * of the views in case of a custom layout.
     *
     * @param myAdapter       the existing base adapter
     * @param myActivity      the current activity
     * @param myAds           list of native ads objects
     * @param layoutResource  the resource of the native ad layout
     * @param iconID          id of the icon view
     * @param titleID         id of title view
     * @param descriptionID   id of the description view
     * @param imageID         id of the image view
     * @param installButtonID id of the install button
     * @param debugMode       enables/disables more detailed info for development purposes
     */
    public NativeAdsAdapter(BaseAdapter myAdapter, Activity myActivity, List<NativeAdUnit> myAds,
                            int layoutResource, int iconID, int titleID, int descriptionID,
                            int imageID, int installButtonID, boolean debugMode) {
        this.userAdapter = myAdapter;
        this.activity = myActivity;
        this.ads = myAds;
        this.adLayout = layoutResource;
        this.iconID = iconID;
        this.titleID = titleID;
        this.descriptionID = descriptionID;
        this.imageID = imageID;
        this.installButtonID = installButtonID;
        this.counter = 0;
        this.debugMode = debugMode;

        for (int i = 0; i < myAdapter.getCount(); i++) {
            mixedList.add(myAdapter.getItem(i));
        }

    }

    /**
     * The constructor of the adapter.
     * Initialize the class parameter myAdapter with the existing adapter,
     * myActivity with the current activity, myAds with the ads retrieved and layoutResource
     * with the resource of the layout to use for each native ad. Also, it adds the items of the
     * existing adapter in a new list of objects, the mixedList. Sets values for two boolean
     * parameters in order to enable/disable to follow the links in an in-app browser and in order to
     * enable/disable to follow redirects in the background (experimental feature). Initializes the ids
     * of the views in case of custom layout.
     *
     * @param myAdapter                   the existing base adapter
     * @param myActivity                  the current activity
     * @param myAds                       list of native ads objects
     * @param layoutResource              the resource of the native ad layout
     * @param followLinksInInappBrowser   enable - disable following redirects after the user has clicked install
     * @param iconID                      id of the icon view
     * @param titleID                     id of title view
     * @param descriptionID               id of the description view
     * @param imageID                     id of the image view
     * @param installButtonID             id of the install button
     * @param debugMode                   enables/disables more detailed info for development purposes
     */
    public NativeAdsAdapter(BaseAdapter myAdapter, Activity myActivity, List<NativeAdUnit> myAds,
                            int layoutResource, boolean followLinksInInappBrowser,
                            int iconID, int titleID, int descriptionID, int imageID,
                            int installButtonID, boolean debugMode) {
        this.userAdapter = myAdapter;
        this.activity = myActivity;
        this.ads = myAds;
        this.adLayout = layoutResource;
        this.followLinksInInappBrowser = followLinksInInappBrowser;
        this.counter = 0;
        this.iconID = iconID;
        this.titleID = titleID;
        this.descriptionID = descriptionID;
        this.imageID = imageID;
        this.installButtonID = installButtonID;
        this.debugMode = debugMode;

        for (int i = 0; i < myAdapter.getCount(); i++) {
            mixedList.add(myAdapter.getItem(i));
        }

    }

    @Override
    public int getCount() {
        int count = 0;
        try {
            count = mixedList.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        return mixedList.get(position);
    }

    @Override
    public long getItemId(int position) {
        boolean isAd = thereIsAdInPosition(position);
        long itemId = -1;

        if (!isAd) {
            itemId = userAdapter.getItemId(position);
        } else {
            try {
                NativeAdUnit nativeAd = (NativeAdUnit) getItem(position);

                if (nativeAd.unitID != null) {
                    itemId = nativeAd.getUnitID();
                } else {
                    itemId = nativeAd.clickURL.hashCode();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return itemId;
    }

    @Override
    public int getViewTypeCount() {
        if (getCount() != 0)
            return getCount();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position - ads.size();
    }

    private boolean thereIsAdInPosition(int position) {

        boolean elementIsAnAd;
        try {
            Object itemAtPosition = getItem(position);
            elementIsAnAd = itemAtPosition.getClass() == NativeAdUnit.class;
        } catch (Exception e) {
            elementIsAnAd = false;
        }

        return elementIsAnAd;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        boolean showAd = thereIsAdInPosition(position);

        if (showAd) {
            // build/return ad view
            NativeAdDisplayHelper nativeAdDisplayHelper = new NativeAdDisplayHelper(activity,
                    followLinksInInappBrowser, iconID, titleID,
                    descriptionID, imageID, installButtonID, debugMode);
            int adPosition = getNativeAdPositionInOriginalList(position);

            convertView = nativeAdDisplayHelper.getAdView(ads.get(adPosition), adLayout);
            if (counter < ads.size() - 1) {
                counter++;
            } else {
                counter = 0;
            }
        } else {
            // return original object from the user's list
            try {
                int positionInOriginalList = getOriginalPositionForElement(position);
                convertView = userAdapter.getView(positionInOriginalList, convertView, parent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return convertView;
    }

    private int getNativeAdPositionInOriginalList(int position) {
        for (int i = 0; i < ads.size(); i++) {
            NativeAdUnit originalItem = ads.get(i);
            if (originalItem == mixedList.get(position)) {
                return i;
            }
        }
        //TODO: handle gracefully
        return -1;

    }

    private int getOriginalPositionForElement(int position) {
        for (int i = 0; i < userAdapter.getCount(); i++) {
            Object originalItem = userAdapter.getItem(i);
            if (originalItem == mixedList.get(position)) {
                return i;
            }
        }
        //TODO: handle gracefully
        return -1;
    }


    /**
     * Method to add the ads in the mixedList (a list of objects) which already
     * contains the content of an existing adapter. It is used to update the ads
     * every time there are new ones.
     *
     * @param ads a list of NativeAdUnit objects, the list of native ads.
     */
    public void updateAds(List<NativeAdUnit> ads) {
        this.ads = ads;
        removeItemsFromMixedList(NativeAdUnit.class);
        for (int i = 0; i < ads.size(); i++) {
            int adPosition = getRandomPosition();
            NativeAdUnit nativeAd = ads.get(i);
            mixedList.add(adPosition, nativeAd);
            if (debugMode) {
                Log.d(this.getClass().getSimpleName(), "Position: " + adPosition + "Added: " + nativeAd.campaignName);
            }
        }

        if (debugMode) {
            Log.d(this.getClass().getSimpleName(), "mixedList size after adding ads: " + mixedList.size());
        }
        this.notifyDataSetChanged();
    }

    private void removeItemsFromMixedList(Class classToRemove) {
        List<Object> newList = new ArrayList<>();
        for (Object item : mixedList) {
            if (item.getClass() != classToRemove) {
                newList.add(item);
            }
        }
        if (debugMode) {
            Log.d(this.getClass().getSimpleName(), "mixedList size: " + newList.size() + " (it was: " + mixedList.size() + ")");
        }

        this.mixedList = newList;
    }

    private int getRandomPosition() {
        int position;
        int listSize = mixedList.size();
        if (listSize == 0){
            return 0;
        }
        Random randomNumber = new Random();
        position = randomNumber.nextInt(listSize);
        return position;
    }

    /**
     * Method to notify the adapter that data set is changed.
     */
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        userAdapter.notifyDataSetChanged();
    }
}
