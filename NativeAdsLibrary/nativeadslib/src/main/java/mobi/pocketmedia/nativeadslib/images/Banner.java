
package mobi.pocketmedia.nativeadslib.images;

import mobi.pocketmedia.nativeadslib.NativeAdImage;

public class Banner extends NativeAdImage {

    @Override
    public ImageType getImageType() {
        return ImageType.banner;
    }

}
