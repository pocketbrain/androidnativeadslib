package mobi.pocketmedia.nativeadslib.exceptions;

import mobi.pocketmedia.nativeadslib.NativeAdsAdapter;

/**
 * This class is used to to throw an exception in case the native ads adapter is null.
 * <p/>
 * Created by Pocket Media on 15-Feb-16.
 */
public class NullNativeAdsAdapterException extends Exception {

    public NullNativeAdsAdapterException(NativeAdsAdapter nativeAdsAdapter, boolean debugMode) {
        if (nativeAdsAdapter == null) {
            try {
                throw this;
            } catch (NullNativeAdsAdapterException e) {
                if (debugMode) {
                    e.printStackTrace();
                }
            }
        }
    }
}
