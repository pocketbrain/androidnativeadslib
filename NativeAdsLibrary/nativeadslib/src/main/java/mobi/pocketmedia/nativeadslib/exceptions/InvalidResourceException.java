package mobi.pocketmedia.nativeadslib.exceptions;

/**
 * This class is used to throw an exception in case the resource to be used for tha native ad is invalid.
 *
 * Created by Pocket Media on 12-Feb-16.
 */
public class InvalidResourceException extends Exception {
}
