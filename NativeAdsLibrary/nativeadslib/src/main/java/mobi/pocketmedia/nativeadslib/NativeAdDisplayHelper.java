package mobi.pocketmedia.nativeadslib;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Random;

import mobi.pocketmedia.nativeadslib.exceptions.InvalidResourceException;

/**
 * This class is used as a helper to display the ads.
 * <p/>
 * Created by Pocket Media on 18-Jan-16.
 */
public class NativeAdDisplayHelper {

    public ImageView defaultIconView;
    public TextView titleView;
    public TextView descriptionView;
    public ImageView bigImageView;
    public Button installButton;
    public Random randomNumber;

    private LayoutInflater inflater;
    private Activity activity;
    private View nativeAdView;
    private int iconID = 0;
    private int titleID = 0;
    private int descriptionID = 0;
    private int imageID = 0;
    private int installButtonID = 0;

    private boolean followLinksInInAppBrowser = true;
    private boolean debugMode = false;

    public NativeAdDisplayHelper(Activity activity) {
        this.activity = activity;
        this.inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        nativeAdView = new View(activity.getApplicationContext());
        randomNumber = new Random();
    }

    public NativeAdDisplayHelper(Activity activity, boolean debugMode) {
        this.activity = activity;
        this.inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.debugMode = debugMode;
        nativeAdView = new View(activity.getApplicationContext());
        randomNumber = new Random();
    }

    public NativeAdDisplayHelper(Activity a, boolean followLinksInInAppBrowser, boolean debugMode) {
        this.activity = a;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.followLinksInInAppBrowser = followLinksInInAppBrowser;
        this.debugMode = debugMode;
        nativeAdView = new View(activity.getApplicationContext());
        randomNumber = new Random();
    }

    public NativeAdDisplayHelper(Activity activity, int iconID, int titleID, int descriptionID,
                                 int imageID, int installButtonID, boolean debugMode) {
        this.activity = activity;
        this.inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.iconID = iconID;
        this.titleID = titleID;
        this.descriptionID = descriptionID;
        this.imageID = imageID;
        this.installButtonID = installButtonID;
        this.debugMode = debugMode;
        nativeAdView = new View(activity.getApplicationContext());
        randomNumber = new Random();
    }

    public NativeAdDisplayHelper(Activity activity, boolean followLinksInInAppBrowser,
                                 int iconID, int titleID, int descriptionID, int imageID, int installButtonID, boolean debugMode) {
        this.activity = activity;
        this.inflater = (LayoutInflater) this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.followLinksInInAppBrowser = followLinksInInAppBrowser;
        this.iconID = iconID;
        this.titleID = titleID;
        this.descriptionID = descriptionID;
        this.imageID = imageID;
        this.installButtonID = installButtonID;
        this.debugMode = debugMode;
        nativeAdView = new View(activity.getApplicationContext());
        randomNumber = new Random();
    }

    /**
     * Method to inflate the layout that the native ads are going to use.
     *
     * @param displayedAd a NativeAdUnit object
     * @param resource    the resource of the ad layout
     * @return a ViewGroup
     */
    public View getAdView(final NativeAdUnit displayedAd, int resource) {

        if (resource != 0) {
            nativeAdView = inflater.inflate(resource, null, false);
        } else {
            InvalidResourceException invalidResourceException = new InvalidResourceException();
            try {
                throw invalidResourceException;
            } catch (InvalidResourceException e) {
                if (debugMode) {
                    e.printStackTrace();
                }
            }
        }
        return displayAd(displayedAd, resource);
    }

    /**
     * Method to display the ads.
     *
     * @param displayedAd a NativeAdUnit object
     * @param resource    the resource of the ad layout
     */
    public View displayAd(final NativeAdUnit displayedAd, final int resource) {

        if (resource != 0) {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (resource == mobi.pocketmedia.nativeadslib.R.layout.pocket_native_ad_layout_simple_item) {

                        titleView = (TextView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_title);
                        descriptionView = (TextView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_description);
                        defaultIconView = (ImageView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_icon);

                        installButton = (Button) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.install_button);
                        installButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                openAd(displayedAd);
                            }
                        });
                        if (!TextUtils.isEmpty(displayedAd.getActionText())){
                            installButton.setText(displayedAd.getActionText());
                        }

                        titleView.setText(displayedAd.getCampaignDescription());
                        descriptionView.setText(displayedAd.getCampaignDescription());
                        Picasso.with(activity).load(displayedAd.getImageUrl()).placeholder(mobi.pocketmedia.nativeadslib.R.drawable.no_photo).into(defaultIconView);

                    } else if (resource == mobi.pocketmedia.nativeadslib.R.layout.pocket_native_ad_layout_big_image) {

                        titleView = (TextView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_title);
                        descriptionView = (TextView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_description);
                        bigImageView = (ImageView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_image);
                        defaultIconView = (ImageView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_icon);

                        titleView.setText(displayedAd.getCampaignName());
                        descriptionView.setText(displayedAd.getCampaignDescription());
                        Picasso.with(activity).load(displayedAd.getImageUrl()).placeholder(mobi.pocketmedia.nativeadslib.R.drawable.no_photo).into(defaultIconView);
                        String bigImageUrl = "http://lorempixel.com/400/200/technics/" + randomNumber.nextInt(10);
                        if (displayedAd.getImages().getBanner() != null){
                            bigImageUrl = displayedAd.getImages().getBanner().getUrl();
                        }
                        Picasso.with(activity).load(bigImageUrl).into(bigImageView);

                        installButton = (Button) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.install_button);
                        installButton.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                openAd(displayedAd);
                            }
                        });
                        if (!TextUtils.isEmpty(displayedAd.getActionText())){
                            installButton.setText(displayedAd.getActionText());
                        }

                    } else if (resource == mobi.pocketmedia.nativeadslib.R.layout.pocket_native_ad_layout_fullscreen) {

                        titleView = (TextView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_title);
                        descriptionView = (TextView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_description);
                        bigImageView = (ImageView) nativeAdView.findViewById(mobi.pocketmedia.nativeadslib.R.id.pocket_native_ad_image);

                        nativeAdView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                openAd(displayedAd);
                            }
                        });



                        titleView.setText(displayedAd.campaignName);
                        descriptionView.setText(displayedAd.campaignDescription);

                        String bigImageUrl = "http://lorempixel.com/400/200/technics/" + randomNumber.nextInt(10);
                        if (displayedAd.getImages().getBanner() != null){
                            bigImageUrl = displayedAd.getImages().getBanner().getUrl();
                        }
                        Picasso.with(activity).load(bigImageUrl).into(bigImageView);

                    } else {
                        try {
                            titleView = (TextView) nativeAdView.findViewById(titleID);
                            titleView.setText(displayedAd.campaignName);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        try {
                            descriptionView = (TextView) nativeAdView.findViewById(descriptionID);
                            descriptionView.setText(displayedAd.campaignDescription);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        try {
                            defaultIconView = (ImageView) nativeAdView.findViewById(iconID);
                            Picasso.with(activity).load(displayedAd.getImageUrl()).placeholder(mobi.pocketmedia.nativeadslib.R.drawable.no_photo).into(defaultIconView);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        try {
                            bigImageView = (ImageView) nativeAdView.findViewById(imageID);

                            String bigImageUrl = "http://lorempixel.com/400/200/technics/" + randomNumber.nextInt(10);
                            if (displayedAd.getImages().getBanner() != null){
                                bigImageUrl = displayedAd.getImages().getBanner().getUrl();
                            }
                            Picasso.with(activity).load(bigImageUrl).into(bigImageView);

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        try {
                            installButton = (Button) nativeAdView.findViewById(installButtonID);
                            installButton.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    openAd(displayedAd);
                                }
                            });
                            if (!TextUtils.isEmpty(displayedAd.getActionText())){
                                installButton.setText(displayedAd.getActionText());
                            }
                        } catch (Exception ex) {
                            nativeAdView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    openAd(displayedAd);
                                }
                            });
                        }
                    }
                }
            });
        }
        return nativeAdView;
    }

    /**
     * Method to open the Google Play Store app page, with an in-app
     * browser to follow the ads redirects until the user is taken to
     * the final url, for the best possible user experience
     *
     * @param displayedAd a NativeAdUnit object
     */
    public void openAd(NativeAdUnit displayedAd) {

        if (displayedAd != null && displayedAd.clickURL != null) {
            if (followLinksInInAppBrowser && !displayedAd.getShouldBeManagedExternally()) {
                openWebView(displayedAd.clickURL);
            } else {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(displayedAd.clickURL));
                activity.startActivity(browserIntent);
                activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_up_out);
            }
        }
    }

    private void openWebView(String url) {
        Intent webViewIntent = new Intent(activity.getApplicationContext(), WebViewActivity.class);
        webViewIntent.putExtra("url", url);
        activity.startActivity(webViewIntent);
        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_up_out);

        if (debugMode) {
            Log.d(getClass().getSimpleName(), "Open Ad - web view");
        }
    }
}
