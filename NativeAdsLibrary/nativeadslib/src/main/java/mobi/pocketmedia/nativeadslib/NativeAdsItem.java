package mobi.pocketmedia.nativeadslib;

/**
 * This class is used to set the adType of each item. The default value of an item is NOT_AD.
 *
 * Created by Pocket Media on 29/07/15.
 */
public class NativeAdsItem {
    public NativeAdsConstants.AdType adType = NativeAdsConstants.AdType.NOT_AD;
}
