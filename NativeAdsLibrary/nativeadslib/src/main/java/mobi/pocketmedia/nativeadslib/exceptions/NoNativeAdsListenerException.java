package mobi.pocketmedia.nativeadslib.exceptions;

/**
 * This class is used to throw an exception in case the native ad listener is null.
 * <p/>
 * Created by Pocket Media on 11-Feb-16.
 */
public class NoNativeAdsListenerException extends Exception {
}
