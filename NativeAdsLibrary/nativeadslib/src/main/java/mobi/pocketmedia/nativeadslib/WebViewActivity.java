package mobi.pocketmedia.nativeadslib;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/**
 * Activity that generates a webview client to show the native ad landing page of a specific campaign.
 * In case the landing page is a play store url it will launch the play store app, otherwise it will
 * launch a simple android webview.
 *
 * Created by Pocket Media on 12-Jan-16.
 */
public class WebViewActivity extends Activity {

    private Activity activity;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_store_webview);

        activity = this;

        final Intent intent = getIntent();
        WebView webView = (WebView) findViewById(R.id.webview);

        WebViewClient webViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (isGooglePlayInstalled(activity.getApplicationContext())) {
                    if (url != null && (url.startsWith("market://") || url.contains("play.google.com"))) {
                        view.getContext().startActivity(
                                new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        activity.finish();

                        Log.d(getClass().getSimpleName(), "Open Ad - play store");

                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressDialog.dismiss();
            }
        };


        webView.setWebViewClient(webViewClient);
        webView.loadUrl(intent.getStringExtra("url"));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.goBack();

        progressDialog = ProgressDialog.show(activity, "", activity.getResources().getString(R.string.loading), false, true);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                activity.finish();
            }
        });
    }

    /**
     * Method to verify that play store app is installed
     *
     * @param context the application context
     * @return a boolean, if true play store app is installed.
     */
    public boolean isGooglePlayInstalled(Context context) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed;
        try {
            PackageInfo info = pm.getPackageInfo("com.android.vending", PackageManager.GET_ACTIVITIES);
            String label = (String) info.applicationInfo.loadLabel(pm);
            app_installed = (label != null && !label.equals("Market"));
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
            Toast.makeText(activity.getApplicationContext(), activity.getResources().getString(R.string.no_playstore), Toast.LENGTH_SHORT).show();
        }
        return app_installed;
    }
}
