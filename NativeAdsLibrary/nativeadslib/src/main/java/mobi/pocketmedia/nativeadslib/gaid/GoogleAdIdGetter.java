package mobi.pocketmedia.nativeadslib.gaid;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;

import mobi.pocketmedia.nativeadslib.NativeAdsConstants;


/**
 * This class is used to get the user's google advertising id.
 * <p/>
 * Created by Pocket Media on 17-Dec-15.
 */
public class GoogleAdIdGetter {
    public AdvertisingIdClient.Info googleAdInfo;
    public Context myContext;
    private SharedPreferences sharedPreferences;
    private DeviceIdentifierListener listener;

    public GoogleAdIdGetter(Context context) {
        this.myContext = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public GoogleAdIdGetter(Context context, DeviceIdentifierListener listener) {
        this.myContext = context;
        this.listener = listener;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Method to get the Google Advertising Identifier (gaid) stored in Shared Preferences. You should
     * invoke this method whenever you want to retrieve and use the gaid.
     *
     * @return the Google Advertising Identifier as a string
     */
    public String getGoogleAdID() throws GooglePlayServicesNotAvailableException {

        String myGoogleAdID = sharedPreferences.getString(NativeAdsConstants.GOOGLE_AD_ID_KEY, "");

        if (myGoogleAdID.equals("")) {
            updateIdentifier(true);
        }

        return myGoogleAdID;
    }

    /**
     * Method to execute the GetGoogleAdIdAsync in order to get the gaid. This method should be invoked
     * in your application class in the onCreate() method in order to generate GetGoogleAsync and store
     * in Shared Preferences the gaid as a String.
     *
     * @param forceRetrieve boolean parameter to force the code in doInBackground method to be executed
     */
    public void updateIdentifier(boolean forceRetrieve) {
        GetGoogleAdIdAsync getGoogleAdIdAsync = new GetGoogleAdIdAsync(forceRetrieve, listener);
        getGoogleAdIdAsync.execute();
    }

    /**
     * Async task to retrieve the Google Advertising Identifier from google play services
     */
    private class GetGoogleAdIdAsync extends AsyncTask<Boolean, Void, String> {
        private String googleAdId = "";
        private boolean forceRetrieval = true;
        private DeviceIdentifierListener listener;

        GetGoogleAdIdAsync(boolean forceRetrieval, DeviceIdentifierListener listener) {
            this.forceRetrieval = forceRetrieval;
            this.listener = listener;
        }

        @Override
        protected String doInBackground(Boolean... params) {
            SharedPreferences.Editor editor;

            if (forceRetrieval) {

                try {
                    googleAdInfo = AdvertisingIdClient.getAdvertisingIdInfo(myContext);
                    googleAdId = googleAdInfo.getId();

                    if (!TextUtils.isEmpty(googleAdId)) {
                        editor = sharedPreferences.edit();
                        editor.putString("googleAdID", googleAdId);
                        editor.apply();
                    }

                } catch (IOException | GooglePlayServicesNotAvailableException |
                        GooglePlayServicesRepairableException e) {
                    if (listener != null) {
                        listener.onDeviceIdNotAvailable(e);
                    }
                }
            }

            Log.i("GOOGLE_ADVERTISING_ID", ("GoogleAdId: ").concat(googleAdId));

            return googleAdId;
        }

    }
}
