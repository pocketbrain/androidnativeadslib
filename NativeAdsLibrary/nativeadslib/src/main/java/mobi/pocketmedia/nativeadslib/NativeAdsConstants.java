package mobi.pocketmedia.nativeadslib;

import android.os.Build;

/**
 * Declaration and initialization of static variables.
 *
 * Created by Pocket Media on 04-Jan-16.
 */
public class NativeAdsConstants {

    public static final int STATUS_OK = 200;
    public static final String NATIVE_AD_REQUEST = "NativeAdRequest";
    public static final String ANDROID_VERSION = Build.VERSION.RELEASE;
    public static final String DEVICE_MODEL = Build.MODEL;
    public static final String GOOGLE_AD_ID_KEY = "googleAdID";


    /**
     * AdType enumeration, AD or NOT_AD
     */
    public enum AdType {
        NOT_AD, AD
    }
}
