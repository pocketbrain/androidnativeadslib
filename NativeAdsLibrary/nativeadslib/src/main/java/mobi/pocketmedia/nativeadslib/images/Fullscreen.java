package mobi.pocketmedia.nativeadslib.images;


import mobi.pocketmedia.nativeadslib.NativeAdImage;

public class Fullscreen extends NativeAdImage {

    @Override
    public ImageType getImageType() {
        return ImageType.fullscreen;
    }

}
