
package mobi.pocketmedia.nativeadslib;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import mobi.pocketmedia.nativeadslib.images.Banner;
import mobi.pocketmedia.nativeadslib.images.Fullscreen;
import mobi.pocketmedia.nativeadslib.images.HqIcon;
import mobi.pocketmedia.nativeadslib.images.Icon;


/**
 * This class holds the 4 possible kinds of images that a NativeAdUnit can have.
 *
 */

public class NativeAdImages {

    @SerializedName("banner")
    @Expose
    private Banner banner;

    @SerializedName("icon")
    @Expose
    private Icon icon;

    @SerializedName("hq_icon")
    @Expose
    private HqIcon hqIcon;

    @SerializedName("fullscreen")
    @Expose
    private Fullscreen fullscreen;
    /**
     * 
     * @return
     *     The banner
     */
    public NativeAdImage getBanner() {
        return banner;
    }

    /**
     * 
     * @param banner
     *     The banner
     */
    public void setBanner(Banner banner) {
        this.banner = banner;
    }

    /**
     * 
     * @return
     *     The icon
     */
    public NativeAdImage getIcon() {
        return icon;
    }

    /**
     * 
     * @param icon
     *     The icon
     */
    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public NativeAdImage getHq_icon() {
        return hqIcon;
    }

    public void setHq_icon(HqIcon hq_icon) {
        this.hqIcon = hq_icon;
    }

    public NativeAdImage getFullscreen() {
        return fullscreen;
    }

    public void setFullscreen(Fullscreen fullscreen) {
        this.fullscreen = fullscreen;
    }
}
