package mobi.pocketmedia.nativeadslib.gaid;

/**
 * Callback listener for the device identifier.
 *
 * Created by adrian on 21/01/16.
 */
public interface DeviceIdentifierListener {

    /** Method to be implemented in case there is no device id available.
     *
     * @param e the exception to be thrown
     */
    void onDeviceIdNotAvailable(Exception e);
}
