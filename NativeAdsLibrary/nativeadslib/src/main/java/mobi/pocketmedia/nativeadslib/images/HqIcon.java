package mobi.pocketmedia.nativeadslib.images;


import mobi.pocketmedia.nativeadslib.NativeAdImage;

public class HqIcon extends NativeAdImage {

    @Override
    public ImageType getImageType() {
        return ImageType.hqIcon;
    }

}
