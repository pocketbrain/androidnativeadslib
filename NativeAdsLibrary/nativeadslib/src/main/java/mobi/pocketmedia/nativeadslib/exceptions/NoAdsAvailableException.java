package mobi.pocketmedia.nativeadslib.exceptions;

/**
 * This class is used to throw an exception in case we retrieved an empty json.
 *
 * Created by Pocket Media on 22-Jan-16.
 */
public class NoAdsAvailableException extends Exception{
}
