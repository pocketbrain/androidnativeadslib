package mobi.pocketmedia.nativeadslib.demo;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import mobi.pocketmedia.nativeadslib.gaid.DeviceIdentifierListener;
import mobi.pocketmedia.nativeadslib.gaid.GoogleAdIdGetter;

/**
 * Created by Pocket Media on 09-Dec-15.
 */
public class SampleApplication extends Application implements DeviceIdentifierListener {

    public static String DUMMY_DATA = "dummy_data.json";
    public static String POSTS_ASYNC = "PostsAsync";

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        /**
         * New GoogleAdIdGetter instance used to get the gaid
         */
        GoogleAdIdGetter getGoogleAdID = new GoogleAdIdGetter(getApplicationContext(), this);
        getGoogleAdID.updateIdentifier(true);
    }

    @Override
    public void onDeviceIdNotAvailable(Exception e) {
        //we want to display a toast, we need to use a UI thread
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Could not get device identifier. Are Google Play Services available?", Toast.LENGTH_LONG).show();
            }
        });
        e.printStackTrace();
    }
}
