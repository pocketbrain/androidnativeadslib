package mobi.pocketmedia.nativeadslib.demo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by adrian on 06/09/16.
 */
public class AboutAppFragment extends Fragment {

    public ViewGroup myView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        myView = (ViewGroup) inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, myView);

        return myView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        NativeAdsFragment nativeAdsFragment = new NativeAdsFragment();
        MainActivity mainActivity = (MainActivity) getActivity();
        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        switch (id) {
            case R.id.simple_ad:
                editor.putInt(NativeAdsFragment.LAYOUT_RESOURCE, R.layout.custom_native_ad_layout_simple_item).apply();
                mainActivity.changeFragment(nativeAdsFragment);
                return true;
            case R.id.big_image_ad:
                editor.putInt(NativeAdsFragment.LAYOUT_RESOURCE, R.layout.custom_native_ad_layout_big_image).apply();
                mainActivity.changeFragment(nativeAdsFragment);
                return true;
            case R.id.fullscreen_ad:
                editor.putInt(NativeAdsFragment.LAYOUT_RESOURCE, R.layout.custom_native_ad_layout_fullscreen).apply();
                mainActivity.changeFragment(nativeAdsFragment);
                return true;
            case R.id.about_app:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @OnClick(R.id.imgLogo)
    public void openMainWebsiteURL(){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.pocketmedia.mobi"));
        startActivity(browserIntent);
    }


    @OnClick(R.id.btnRepository)
    public void openRepositoryURL(){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://bitbucket.org/pocketbrain/androidnativeadslib"));
        startActivity(browserIntent);
    }

    @OnClick(R.id.btnWebsite)
    public void openWebsiteURL(){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://nativeads.pocketmedia.mobi"));
        startActivity(browserIntent);
    }

}
