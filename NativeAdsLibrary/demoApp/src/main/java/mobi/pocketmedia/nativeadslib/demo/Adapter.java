package mobi.pocketmedia.nativeadslib.demo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mobi.pocketmedia.nativeadslib.NativeAdsItem;

/**
 * This the adapter that is used in the demoApp for the population of the listview.
 * Created by Pocket Media on 23-Dec-15.
 */
public class Adapter extends BaseAdapter {
    private Activity activity;
    protected List<NativeAdsItem> data;

    private static LayoutInflater inflater = null;

    public Adapter(Activity a, List<NativeAdsItem> l) {
        activity = a;
        data = l;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        convertView = inflater.inflate(R.layout.item_layout, null);
        viewHolder = new ViewHolder();

        viewHolder.nameView = (TextView) convertView.findViewById(R.id.title);
        viewHolder.descriptionView = (TextView) convertView.findViewById(R.id.description);
        viewHolder.imageView = (ImageView) convertView.findViewById(R.id.image);

        convertView.setTag(viewHolder);

        PostAttributes post = (PostAttributes) data.get(position);

        viewHolder.nameView.setText(post.name);
        viewHolder.descriptionView.setText(post.description);
        Picasso.with(activity).load(post.artworkUrl).placeholder(R.drawable.no_photo).into(viewHolder.imageView);

        return convertView;
    }

    static class ViewHolder {
        TextView nameView;
        TextView descriptionView;
        ImageView imageView;
    }
}
