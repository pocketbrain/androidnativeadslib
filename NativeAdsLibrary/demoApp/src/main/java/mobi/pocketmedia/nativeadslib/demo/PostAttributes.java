package mobi.pocketmedia.nativeadslib.demo;

import mobi.pocketmedia.nativeadslib.NativeAdsConstants;
import mobi.pocketmedia.nativeadslib.NativeAdsItem;

/**
 * Created by Pocket Media on 11-Jun-15.
 */
public class PostAttributes extends NativeAdsItem {

    //Used in PostFetcher Async
    public String artworkUrl;
    public String name;
    public String description;

    public PostAttributes(){
        adType =  NativeAdsConstants.AdType.NOT_AD;
    }
}
