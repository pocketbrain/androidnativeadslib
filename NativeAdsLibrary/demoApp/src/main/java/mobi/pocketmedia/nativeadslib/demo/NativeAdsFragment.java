package mobi.pocketmedia.nativeadslib.demo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import mobi.pocketmedia.nativeadslib.NativeAdRequest;
import mobi.pocketmedia.nativeadslib.NativeAdUnit;
import mobi.pocketmedia.nativeadslib.NativeAdsAdapter;
import mobi.pocketmedia.nativeadslib.NativeAdsItem;
import mobi.pocketmedia.nativeadslib.NativeAdsRequestListener;


/**
 * Created by Pocket Media on 5/28/2015.
 * <p/>
 * A placeholder fragment containing a simple view.
 */
public class NativeAdsFragment extends Fragment implements NativeAdsRequestListener {

    public static final String LAYOUT_RESOURCE = "resource";

    public ViewGroup myView;
    public ListView myListView;
    //adapter to hold both the posts and the native ads
    //it uses the original ListAdapter, wrapping it
    private NativeAdsAdapter nativeAdsAdapter;
    public ProgressDialog dialog;
    private List<PostAttributes> postsList;

    private NativeAdRequest nativeAdRequest;
    private Parcelable state;
    private SharedPreferences sharedPreferences;
    private int resource;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        myView = (ViewGroup) inflater.inflate(R.layout.fragment_main, container, false);

        PostsAsync postsAsync = new PostsAsync();
        postsAsync.execute();

        sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        resource = sharedPreferences.getInt(LAYOUT_RESOURCE, 0);

        return myView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        NativeAdsFragment nativeAdsFragment = new NativeAdsFragment();
        MainActivity mainActivity = (MainActivity) getActivity();
        SharedPreferences.Editor editor = sharedPreferences.edit();

        switch (id) {
            case R.id.simple_ad:
                editor.putInt(LAYOUT_RESOURCE, R.layout.custom_native_ad_layout_simple_item).apply();
                mainActivity.changeFragment(nativeAdsFragment);
                return true;
            case R.id.big_image_ad:
                editor.putInt(LAYOUT_RESOURCE, R.layout.custom_native_ad_layout_big_image).apply();
                mainActivity.changeFragment(nativeAdsFragment);
                return true;
            case R.id.fullscreen_ad:
                editor.putInt(LAYOUT_RESOURCE, R.layout.custom_native_ad_layout_fullscreen).apply();
                mainActivity.changeFragment(nativeAdsFragment);
                return true;
            case R.id.about_app:
                AboutAppFragment aboutFragment = new AboutAppFragment();
                mainActivity.changeFragment(aboutFragment);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {

        if (state != null) {
            myListView.onRestoreInstanceState(state);
        }
        super.onResume();
        downloadAds();
    }

    @Override
    public void onPause() {
        if (myListView != null) {
            state = myListView.onSaveInstanceState();
        }
        super.onPause();
    }

    private void errorJSONParse(Exception ex, String TAG) {
        Log.e(TAG, "Failed to parse JSON due to: " + ex);
        ex.printStackTrace();
        failedLoadingPosts();
        dismissProgressDialog();
    }

    /**
     * -------------- NATIVE ADS METHODS ----------------
     */

    /**
     * Method to download the ads. Initializing the ad request.
     */
    private void downloadAds() {
        Log.d(getClass().getSimpleName(), "Starting Ads download");
        /**
        We download ads for:
         - application context
         - 3 ads
         - placement id 4bea87cd5549527b10d2886061121d0f8e22071b (so you can know which placements perform better)
         - debug mode enabled - disable in production!
         */
        nativeAdRequest = new NativeAdRequest(
                getActivity().getApplicationContext(),
                3,
                "4bea87cd5549527b10d2886061121d0f8e22071b",
                true
        );

        if (resource == R.layout.custom_native_ad_layout_big_image){
            nativeAdRequest.imageFilter = NativeAdRequest.ImageTypeFilter.bigImages;
        }else if (resource == R.layout.custom_native_ad_layout_fullscreen){
            nativeAdRequest.imageFilter = NativeAdRequest.ImageTypeFilter.bigImages;
        }

        nativeAdRequest.initAdRequest(this);
    }

    /**
     * Override method from NativeAdRequestListener interface. Indicates that the request has started.
     *
     * @param nativeAdRequest the request to the app wall
     */
    @Override
    public void onAdRequestStarted(NativeAdRequest nativeAdRequest) {
        Log.d(getClass().getSimpleName(), "onAdRequestStarted");
    }

    /**
     * Override method from NativeAdRequestListener interface. Indicates that the request was successful.
     *
     * @param nativeAdRequest the request to the app wall
     * @param ads             the list of native ads
     */
    @Override
    public void onAdRequestSuccess(NativeAdRequest nativeAdRequest, List<NativeAdUnit> ads) {
        Log.d(getClass().getSimpleName(), "onAdRequestSuccess");
        Log.d(getClass().getSimpleName(), "Ads download/process finished");

        if (!ads.isEmpty() && getActivity() != null && nativeAdsAdapter != null) {
            nativeAdsAdapter.updateAds(ads);
            nativeAdsAdapter.notifyDataSetChanged();
            myListView.invalidateViews();
        }
    }

    /**
     * Override method from NativeAdRequestListener interface. Indicates that the request failed.
     *
     * @param nativeAdRequest the request to the app wall
     * @param error           exception that occurred
     */
    @Override
    public void onAdRequestError(NativeAdRequest nativeAdRequest, Exception error) {
        Log.e(getClass().getSimpleName(), "onAdRequestError");
        error.printStackTrace();
    }


    /**
     * The async task to retrieve the regular posts of the sample project
     */
    private class PostsAsync extends AsyncTask<Void, Void, List<PostAttributes>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = ProgressDialog.show(getActivity(), "",
                    getResources().getString(R.string.loading_list), false, true);

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    PostsAsync.this.cancel(true);

                    MainActivity newActivity = (MainActivity) getActivity();
                    newActivity.onBackPressed();
                }
            });
        }

        @Override
        protected List<PostAttributes> doInBackground(Void... params) {
            try {

                Log.d(getClass().getSimpleName(), "Starting content download");
                InputStream content = getActivity().getAssets().open(SampleApplication.DUMMY_DATA);

                try {

                    Reader reader = new InputStreamReader(content);

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                    Gson gson = gsonBuilder.create();
                    postsList = Arrays.asList(gson.fromJson(reader, PostAttributes[].class));
                    content.close();

                } catch (Exception ex) {
                    errorJSONParse(ex, SampleApplication.POSTS_ASYNC);
                }
                dismissProgressDialog();

                return postsList;

            } catch (Exception ex) {
                errorJSONParse(ex, SampleApplication.POSTS_ASYNC);
            }
            return null;
        }


        @Override
        protected void onPostExecute(final List<PostAttributes> posts) {
            super.onPostExecute(posts);

            if (getActivity() == null || nativeAdRequest == null) {
                // do not process information as we are in an invalid context
                return;
            }

            Log.d(getClass().getSimpleName(), "Content download/process finished");

            myListView = (ListView) myView.findViewById(R.id.listView);

            if (posts != null) {

                try {
                    List<NativeAdsItem> finalList = new LinkedList<>();
                    finalList.addAll(posts);

                    Adapter adapter = new Adapter(getActivity(), finalList);

                    if (getActivity() == null) {
                        return;
                    }

                    if (resource == 0) {

                        nativeAdsAdapter = new NativeAdsAdapter(
                                adapter,
                                getActivity(),
                                nativeAdRequest.getNativeAds(), R.layout.custom_native_ad_layout_simple_item,
                                R.id.native_ad_icon, R.id.native_ad_title, R.id.native_ad_description, R.id.native_ad_image,
                                R.id.install_button, true);
                        myListView.setAdapter(nativeAdsAdapter);

                    } else {
                        nativeAdsAdapter = new NativeAdsAdapter(adapter, getActivity(),
                                nativeAdRequest.getNativeAds(), resource, R.id.native_ad_icon,
                                R.id.native_ad_title, R.id.native_ad_description, R.id.native_ad_image,
                                R.id.install_button, true);
                        myListView.setAdapter(nativeAdsAdapter);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                dismissProgressDialog();
            } else {
                dismissProgressDialog();
            }
        }
    }

    private void failedLoadingPosts() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "Failed to load Posts. Have a look at LogCat.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void dismissProgressDialog() {
        if (getActivity() != null && dialog != null) {
            dialog.dismiss();
        }
    }
}
