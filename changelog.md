# Changelog #

- 1.2.3:
	- when the ad is clicked and the ```NativeAdDisplayHelper.openAd``` method is used, it will honor the value of the ad attribute ```shouldBeManagedExternally```	
- 1.2.2: 
	- added new image fields to the ```NativeAdUnit``` object, of class ```NativeAdImages```, containing the different kind of assets a NativeAdUnit might contain:
		- icon: the default square image for the apps
		- hq icon: same as the icon, but higher quality
		- banner: horizontal image
		- fullscreen: larger image, also in vertical proportion
	- added new attribute ```shouldBeManagedExternally```, to indicate if the offer should be loaded directly in the system browser
	- added new attribute ```actionText``` to indicate the Call To Action text in the install button
	- updated Gson version to 2.7
- 1.2.1:
	- corrected a bug when opening ad urls in an external browser - thanks Lars Christensen from Xoopsoft!
- 1.2.0: 
	- added new layouts ready to be used in external apps. 
	- following ad redirects in a embebed browser
	- added more constructores for NativeAdDisplayHelper clases
	- solved one bug in the URL generation (not URL encoded variables) - thanks Lars Christensen from Xoopsoft!
	- Bugfixes

	